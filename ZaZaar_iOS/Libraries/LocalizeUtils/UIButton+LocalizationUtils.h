//
//  UIButton+LocalizationUtils.h
//  SmartHomeConnect
//
//  Created by Morris Lin on 2017/5/16.
//  Copyright © 2017年 JSW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (LocalizationUtils)

@property (nonatomic, copy) IBInspectable NSString *localizedTitle;

@end
