//
//  LocalizationModel.h
//  Togetrip_iOS
//
//  Created by Morris Lin on 2017/7/11.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <Foundation/Foundation.h>

#define YCLocalizedString(key, comment) [LocalizationModel localizedStringByKey:(key) Comment:(comment)]

@interface LocalizationModel : NSObject

+(NSString *)localizedStringByKey:(NSString *)key Comment:(NSString *)comment;

@end
