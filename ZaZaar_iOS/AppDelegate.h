//
//  AppDelegate.h
//  ZaZaar_iOS
//
//  Created by pomelolin48 on 2018/8/19.
//  Copyright © 2018年 YCLin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

