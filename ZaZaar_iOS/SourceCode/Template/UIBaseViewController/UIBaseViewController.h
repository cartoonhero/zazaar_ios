//
//  UIBaseViewController.h
//  Meet_IM
//
//  Created by Morris Lin on 2018/4/17.
//  Copyright © 2018年 fullerton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBaseViewController : UIViewController

//params
@property (weak, nonatomic) IBOutlet UIView *baseNavigationBar;
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *navLeftButton;
@property(strong,nonatomic)UIViewController *containerViewController;
//method
-(void)basePushToViewController:(UIViewController *)viewController Animated:(BOOL)anmated;
-(void)basePresentViewController:(UIViewController *)viewController Animated:(BOOL)anmated;
-(void)addTarget:(id)aTarget navButtonClickAction:(SEL)selector;
-(void)addTarget:(id)aTarget buttonClickAction:(SEL)selector;
-(void)hideBaseNavigationBar;
-(void)displayBaseNavigationBar;

@end
