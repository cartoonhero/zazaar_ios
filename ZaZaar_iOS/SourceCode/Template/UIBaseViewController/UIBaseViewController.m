//
//  UIBaseViewController.m
//  Meet_IM
//
//  Created by Morris Lin on 2018/4/17.
//  Copyright © 2018年 fullerton. All rights reserved.
//

#import "UIBaseViewController.h"

@interface UIBaseViewController ()
{
    __weak IBOutlet UIView *containerView;
    SEL navButtonClickActionSelector;
    SEL buttonClickActionSelector;
    CGFloat defaultNavHeight;
    __weak IBOutlet NSLayoutConstraint *navBarHeightConstraint;
}
@end

@implementation UIBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_containerViewController.title.length > 0)
    {
        self.navTitleLabel.text = _containerViewController.title;
    }
    defaultNavHeight = navBarHeightConstraint.constant;
    if (_containerViewController != nil)
    {
        for (UIViewController *vc in self.childViewControllers)
        {
            [vc willMoveToParentViewController:nil];
            [vc.view removeFromSuperview];
            [vc removeFromParentViewController];
        }
        [self addChildViewController:_containerViewController];
        _containerViewController.view.frame = containerView.bounds;
        [containerView addSubview:_containerViewController.view];
        [_containerViewController didMoveToParentViewController:self];
    }
}
#pragma mark - public
-(void)basePushToViewController:(UIViewController *)viewController Animated:(BOOL)anmated
{
    UIBaseViewController *newBaseVC = [self.storyboard instantiateViewControllerWithIdentifier:self.restorationIdentifier];
    newBaseVC.containerViewController = viewController;
    [self.navigationController pushViewController:newBaseVC animated:anmated];
}
-(void)basePresentViewController:(UIViewController *)viewController Animated:(BOOL)anmated
{
    UIBaseViewController *newBaseVC = [self.storyboard instantiateViewControllerWithIdentifier:self.restorationIdentifier];
    newBaseVC.containerViewController = viewController;
    [self presentViewController:newBaseVC animated:anmated completion:nil];
}
-(void)hideBaseNavigationBar
{
    navBarHeightConstraint.constant = 0;
    _baseNavigationBar.hidden = YES;
}
-(void)displayBaseNavigationBar
{
    navBarHeightConstraint.constant = defaultNavHeight;
    _baseNavigationBar.hidden = NO;
}
#pragma mark - public

-(void)addTarget:(id)aTarget navButtonClickAction:(SEL)selector
{
    navButtonClickActionSelector = selector;
    if (_containerViewController == nil)
    {
        _containerViewController = aTarget;
    }
}
-(void)addTarget:(id)aTarget buttonClickAction:(SEL)selector
{
    buttonClickActionSelector = selector;
    if (_containerViewController == nil)
    {
        _containerViewController = aTarget;
    }
}
#pragma mark - action
-(IBAction)navButtonClickAction:(UIButton *)sender
{
    if (navButtonClickActionSelector != nil && [_containerViewController respondsToSelector:navButtonClickActionSelector])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [_containerViewController performSelector:navButtonClickActionSelector withObject:sender];
#pragma clang diagnostic pop
    }else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(IBAction)buttonClickAction:(UIButton *)sender
{
    if (buttonClickActionSelector != nil  && [_containerViewController respondsToSelector:buttonClickActionSelector])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [_containerViewController performSelector:buttonClickActionSelector withObject:sender];
#pragma clang diagnostic pop
    }
}

@end
