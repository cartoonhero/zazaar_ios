//
//  UIAvatarView.h
//  CustomViewTest
//
//  Created by Morris Lin on 2017/5/23.
//  Copyright © 2017年 jsw. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAvatarView : UIView

@property (nonatomic, assign) IBInspectable CGFloat innerWidth;
@property (nonatomic, assign) IBInspectable NSString *imageName;
@property (nonatomic, assign) CGFloat corner_radius;

-(void)putAvatarImage:(UIImage *)image;

@end
