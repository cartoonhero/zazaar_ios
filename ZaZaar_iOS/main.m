//
//  main.m
//  ZaZaar_iOS
//
//  Created by pomelolin48 on 2018/8/19.
//  Copyright © 2018年 YCLin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
